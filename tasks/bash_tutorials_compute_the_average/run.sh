#!/bin/bash

read N
sum=0

for line in $(cat /dev/stdin);
do
	sum=$(expr $sum + $line)
done

echo "scale=3;" $sum / $N |bc
