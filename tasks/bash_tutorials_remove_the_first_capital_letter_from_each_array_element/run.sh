#!/bin/bash

file="/dev/stdin"
input=$(cat $file)

for i in "${!input[@]}";
do
	input[$i]=$(echo "${input[$i]}" | sed 's/^././g')


done

echo $(IFS=, ; echo "${input[@]}")

