#!/bin/bash

input=$(cat "/dev/stdin")

echo "scale=4;" "$input" | bc | xargs printf "%.3f" | sed -e 's/\,/\./'

