#!/bin/bash

file="/dev/stdin"

cat $file | uniq -c | awk '{print $1" " $2}' | head -c -2
