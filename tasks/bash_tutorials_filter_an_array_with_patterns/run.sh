#!/bin/bash

file="/dev/stdin"
input=$(cat $file)

for line in $input;
do
if [[ $line != *"a"* ]] && [[ $line != *"A"* ]]; then
  echo $line
fi
done
