#!/bin/bash

file="/dev/stdin"

read -r X
read -r Y
read -r Z

if [[ $X == $Y ]] && [[ $X == $Z ]]; then
	echo "EQUILATERAL" 
elif [[ $X == $Z ]] || [[ $Y == $Z ]] || [[ $X == $Y ]]; then
	echo "ISOSCELES"
else
	echo "SCALENE"
fi
